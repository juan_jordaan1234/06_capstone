'use strict';

let errorFormatter = function () {};

errorFormatter.prototype.getResponse = function (validationResult) {
  let formatted = [];
  console.log('validationResult.errors = ' + JSON.stringify(validationResult.errors, null, '\t'));
  for( let index in validationResult.errors ){
    switch (validationResult.errors[index].properties.type){
        case 'required':
          formatted.push(validationResult.errors[index].path.charAt(0).toUpperCase() + validationResult.errors[index].path.slice(1) + ' is required.');
        break;

        case 'user defined':
          formatted.push(validationResult.errors[index].message.charAt(0).toUpperCase() + validationResult.errors[index].message.slice(1));
        break;

        default:
          return ['Unkown Error type `' + validationResult.errors[index].properties.type + '` received. Please Contact support with this message'];
    }
  }

  return formatted;
};

exports.errorFormatter = new errorFormatter();
