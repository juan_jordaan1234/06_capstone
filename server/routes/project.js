'use strict';

let express = require('express');
let router = express.Router();
let Project = require('../../common/models/project');
let User = require('../../common/models/user');
let bodyParser = require('body-parser');
let Message = require('../../common/models/message');
var errorFormatter = require('./error.format.js').errorFormatter;

router.use(bodyParser.json());
//Verify.verifyOrdinaryUser, Verify.verifyAdmin
router.route('/').post(createProject);
router.route('/:id').get(getProject).put(updateProject);
router.route('/:id/bid').post(createBid).get(getBids);
router.route('/owner/:companyName').get(getCompanyProjects);
router.route('/provider/:userId').get(getProviderProjects);

module.exports = router;

function createProject(req, res, next){
  if(!req.body.createDate) req.body.createDate = new Date();
  //if(!req.body.skill) return res.status(422).json([{message: 'Please choose required skills'}]);
  //if(req.body.skill.length == 0) return res.status(422).json([{message: 'Please choose required skills'}]);
  //console.log('req.body = ' + JSON.stringify(req.body,  null, '\t'));
  //console.log('req.body.skill = ' + JSON.stringify(req.body.skill,  null, '\t'));

  Project.create(req.body, function (err, data){
    if( err ) return res.status(422).json(errorFormatter.getResponse(err));

    var projectSkills = [];
    for(let skill of req.body.skill)
      projectSkills.push(skill._id);

    User.find({skill: { $in: req.body.skill }}, function (err, data){
      if( err ) return res.status(422).json(errorFormatter.getResponse(err));

      // TODO : replace with email template
      for( let user of data ){
        console.log('New Project message created for user ' + user.firstname + ' ' + user.lastname);
        var message = {
          'owner':user._id,
          'sender': req.body.contact,
          'receiver':user._id,
          'title':req.body.title,
          'text':'A new Project has been created that has some of your skills, we thought you might be interested : ' + req.body.description,
        };

        //console.log('new message = ' + JSON.stringify(message, null, '\t'));

        Message.create(message, function (err, data){
          if( err ) return res.status(422).json(errorFormatter.getResponse(err));
        });
      }
    });

    return res.status(201).json({_id: data.id});
  });
};

function getProject(req, res, next){
  Project.findById(req.params.id,  function (err, data) {
    if( err ) return res.status(422).json(errorFormatter.getResponse(err));

    return res.status(200).json(data);
  });
};

// function deleteProject(req, res, next){
//   Project.findByIdAndRemove(req.params.dishId, function (err, res) {
//     if( err ) return res.status(422).json([{message: err}]);
//       return res.status(201).json({id: data._id});
//   });
// };

function updateProject(req, res, next){
  Project.findByIdAndUpdate(
    req.params.id,
    { $set: req.body },
    { new: true },
    function (err, data) {
      if( err ) return res.status(422).json(errorFormatter.getResponse(err));
      res.json(data);
    }
  );
};

function getCompanyProjects(req, res, next){
  Project.find({'company':req.params.companyName}, function (err, data){
    if (err) throw err;
    res.json(data);
  });
};

function getProviderProjects(req, res, next){
  Project.find({'provider':req.params.userId}, function (err, data){
    if (err) throw err;
    res.json(data);
  });
};

function createBid(req, res, next){
  Project.findById(req.params.id, function(err, project){
    if (err) throw err;

    project.bids.push(req.body);
    project.save(function (err, project){
      if( err ) return res.status(422).json(errorFormatter.getResponse(err));

      //console.log('project = ' + JSON.stringify(project, null, '\t'));
      //console.log('project.contact = ' + project.contact._id);
      //console.log('req.body.bidder._id = ' + req.body.bidder._id);

      var message = {
        'owner':project.contact,
        'sender': req.body.bidder._id,
        'receiver':project.contact,
        'title':'New Bid Received',
        'text':'You have received a new bid, open your project in the Dashboard to view it!',
      };

      Message.create(message, function (err, data){
        if( err ) return res.status(422).json(errorFormatter.getResponse(err));
      });

      return res.status(201).json();
      //return res.status(201).json(project._id);
    });
  });
};

function getBids(req, res, next){
  Project.findById(req.params.id, function(err, data){
    if (err) throw err;

    res.json(data.bids);
  });
};
