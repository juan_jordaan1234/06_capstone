'use strict';

let express = require('express');
let router = express.Router();
let Message = require('../../common/models/message');
let bodyParser = require('body-parser');
var errorFormatter = require('./error.format.js').errorFormatter;

router.use(bodyParser.json());

//Verify.verifyOrdinaryUser, Verify.verifyAdmin
router.route('/user/:userId').get(getAllMessages).post(createMessage).delete(deleteMessage);

module.exports = router;

function getAllMessages(req, res, next){
  Message.find({owner:req.params.userId})
  .populate('sender')
  .exec(function (err, data){
    //TODO : Mark sent messages as received
    for(let message of data )
      if( message.status === 'sent')
        message.status = 'received';

    if (err) throw err;
    res.json(data);
  });
};

function createMessage(req, res, next){
  req.body.sentDate = new Date();
  req.body.status = 'sent';

  //save one for the sender
  req.body.owner = req.body.sender;
  Message.create(req.body, function (err, data){
    if( err ) return res.status(422).json(errorFormatter.getResponse(err));

    //save one for the receiver
    req.body.owner = req.body.receiver;
    Message.create(req.body, function (err, data){
      if( err ) return res.status(422).json(errorFormatter.getResponse(err));

      return res.status(201).json({_id: data._id});
    });
  });
};

function deleteMessage(req, res, next){
  //console.log('req.body._id      = ' + JSON.stringify(req.body._id, null, '\t'));
  //console.log('req.params.userId = ' + JSON.stringify(req.params.userId, null, '\t'));
  Message.findOne({'owner':req.params.userId, '_id':req.body._id}, function (err, data){
    if (err) {
      //console.log('response err = ' + JSON.stringify(err, null, '\t'));
      throw err;
    }

    //console.log('response data = ' + JSON.stringify(data, null, '\t'));
    data.remove();
    res.json();
  });
};
