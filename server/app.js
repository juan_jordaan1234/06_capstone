/**
* Module dependencies.
*/

'use strict';

let express = require('express');
let app = express();
//var http = require('http');
let path = require('path');
let fs = require('fs');

let mongoose = require('mongoose');
let config = require('./config/settings');

let bodyParser = require('body-parser');
//let logger = require('morgan');
//let errorHandler = require('errorhandler');

// load in the appropriate order for mongoose.model to load
let index = require('./routes/index');
let country = require('./routes/country');
let skill = require('./routes/skill');
let user = require('./routes/user');
let project = require('./routes/project');
let message = require('./routes/message');

let auth = require('./config/basic-auth');
//let osprey = require('osprey');

let join = require('path').join;

mongoose.connect(config.mongoUrl);
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.on('close', console.error.bind(console, 'connection closed:'));
db.once('open', function () { console.log("Connected correctly to server"); });
//mongoose.Schema.set('toJSON', { virtuals: true });

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.set('views', __dirname + '/../views');
app.use(express.static(path.join(__dirname, '/../client')));
app.use('/bower_components', express.static(path.join(__dirname, '/../bower_components')));
app.use('/app', express.static(path.join(__dirname, '/../client/app')));
app.use('/fonts', express.static(path.join(__dirname, '/../client/fonts')));
app.use('/images', express.static(path.join(__dirname, '/../client/images')));
app.use('/scripts', express.static(path.join(__dirname, '/../client/scripts')));
app.use('/css', express.static(path.join(__dirname, '/../client/css')));

app.set('port', process.env.PORT || 3000);
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);
//app.use(logger('dev'));

// development only
// if ('development' == app.get('env')) {
// 	app.use(errorHandler());
// }

app.use('/', index);
app.use('/api/countries', country);
app.use('/api/users', user);
app.use('/api/skills', skill);
app.use('/api/projects', project);
app.use('/api/messages', message);

// path = join(__dirname, 'assets', '../raml/api.raml');
// osprey.loadFile(path)
// .then(function (middleware) {
// 	app.use(middleware)
// 	app.use(function (err, req, res, next) {
//     // Handle errors.
// 		console.log('raml osprey err = ' + err );
//   })
//   app.listen(3000)
// })

//expressListRoutes('API:', router );

//console.log(app._router.stack);

// Secure traffic only
// app.all('*', function(req, res, next){
//   console.log('req start: ',req.secure, req.hostname, req.url, app.get('port'));
//   if (req.secure) return next();
//
//   res.redirect('https://'+req.hostname+':'+app.get('secPort')+req.url);
// });

// app.use(auth);
// app.use(function(err,req,res,next){
//   res.writeHead(err.status || 500, { 'WWW-Authenticate': 'Basic', 'Content-Type': 'text/plain' });
//   res.end(err.message);
// });

// error handlers
//catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json([{ message: err.message, error: err }]);
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({ message: err.message, error: {} });
});

/**
 * Event listener for HTTP server "error" event.
 */
// function onError(error) {
//   if (error.syscall !== 'listen') throw error;
//
//   var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;
//
//   // handle specific listen errors with friendly messages
//   switch (error.code) {
//     case 'EACCES':
//       console.error(bind + ' requires elevated privileges');
//       process.exit(1);
//     break;
//     case 'EADDRINUSE':
//       console.error(bind + ' is already in use');
//       process.exit(1);
//     break;
//     default:
//       throw error;
//   }
// };

// var httpServer = http.createServer(app);
// httpServer.listen(app.get('port'), '0.0.0.0', function() {
// 	console.log('(HTTP)  Server listening on port ' + app.get('port'));
// });
//
// httpServer.on('error', onError);

module.exports = app;
