var session = require('express-session');
var FileStore = require('session-file-store')(session);
var basicAuth = require('./basic-auth');
var config = require('./settings');

module.exports = (
  function(){
    var sessions = {
      getSessionConfig: session({
        name: 'session-id',
        secret: config.secretKey,
        saveUninitialized: true,
        resave: true,
        store: new FileStore()
      }),
      getSessionAuth: sessionAuth
    };

    return sessions;

    function sessionAuth(req, res, next){
      console.log('req.headers = ' + JSON.stringify(req.headers, null, '\t') );

      if (!req.session.user){
        if( basicAuth(req, res, next)){
          console.log('passed auth');

          req.session.user = 'admin';
        }
        else {
          console.log('failed auth');
        }
      }
      else{
        if (req.session.user === 'admin') {
          console.log('req.session: ' + JSON.stringify(req.session, null, '\t') );
          next();
        }
        else {
          var err = new Error('You are not authenticated!');
          err.status = 401;
          next(err);
        }
      }
    };
  }
)();
