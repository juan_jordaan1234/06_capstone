module.exports = function auth (req, res, next){
  console.log('req.headers.authorization = ' + JSON.stringify(req.headers.authorization, null, '\t') );

  var authHeader = req.headers.authorization;

  if(!authHeader){
    // get user to type in username and password so we can authenticate them
    var err = new Error('You are not authenticated!');
    err.status = 401;
    next(err);
    return false;
  }

  var auth = new Buffer(authHeader.split(' ')[1], 'base64').toString().split(':');
  var user = auth[0];
  var pass = auth[1];

  if(user == 'admin' && pass == 'password'){
    next();
    return true;
  }
  else{
    var err = new Error('You are not authenticated!');
    err.status = 401;
    next(err);
    return false;
  }
};
