'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let Email = require('mongoose-type-email');
let Country = mongoose.model('Country');
let Skill = mongoose.model('Skill');
let ObjectId = mongoose.Schema.Types.ObjectId;
var uniqueValidator = require('mongoose-unique-validator');

//var passportLocalMongoose = require('passport-local-mongoose');

let UserSchema = new Schema( require('./user.json'), { timestamps: true }  );

UserSchema.methods.getFullName = function() {
  return (this.firstname + ' ' + this.lastname);
};

UserSchema.plugin(uniqueValidator);

//User.plugin(passportLocalMongoose);

module.exports = mongoose.model('User', UserSchema);
