/*global describe, it, before */
/**
 * REST API Tests
 */
var request = require('supertest');
var app = require('../app');
var assert = require('assert');

before(function importSampleData(done) {
  this.timeout(50000);
  if (app.importing) {
    app.on('import done', done);
  } else {
    done();
  }
});

function json(verb, url) {
  return request(app)[verb](url)
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/);
}

describe('REST', function() {
  this.timeout(30000);

  describe('POST /api/message_tmps', function() {
    it('should create a new message', function(done) {
      json('post', '/api/message_tmps')
      .send({ text: 'Hello World' })
      .expect(200)
      .end(function(err, res) {
        console.log( 'res.body = ' + JSON.stringify(res.body) );
        assert(typeof res.body === 'object');
        assert(res.body.id, 'must have an id');
        carId = res.body.id;
        done();
      });
    });
  });

  describe('POST /api/message_tmps', function() {
    it('should create a new message', function(done) {
      json('post', '/api/message_tmps')
        .send({ text: 'Hello World2' })
        .expect(200)
        .end(function(err, res) {
          console.log( 'res.body = ' + JSON.stringify(res.body) );
          assert(typeof res.body === 'object');
          assert(res.body.id, 'must have an id');
          carId = res.body.id;
          done();
        });
    });
  });

  describe('GET /api/message_tmps', function() {
    it('should return a list of all messages', function(done) {
      json('get', '/api/message_tmps')
        .expect(200)
        .end(function(err, res) {
          console.log( 'res.body = ' + JSON.stringify(res.body) );
          assert(Array.isArray(res.body));
          assert(res.body.length);

          done();
        });
    });
  });
});
