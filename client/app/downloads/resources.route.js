(function() {
  'use strict';

  angular
  .module('app.resources')
  .run(getRoute);

  getRoute.$inject = ['routeUtil'];

  function getRoute(routeUtil) {
    //console.log('sending route to util');
    routeUtil.addState(
      {
        name: 'resources',
        config: {
          url: '/resources',
          views: {
            'content': {
              templateUrl: '/app/downloads/resources.html',
              controller: 'ResourcesController',
              controllerAs: 'vm',
              title: 'Resources'
            }
          }
        }
      }
    )
  }
})();
