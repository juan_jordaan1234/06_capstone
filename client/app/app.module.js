(function() {
  'use strict';

  angular.module('app', ['app.util', 'app.layout', 'app.home', 'app.resources', 'app.members', 'app.dashboard', 'app.menu', 'app.contact', 'app.about']);
})();
