(function() {
  'use strict';

  //'ui.router', 'ngResource'
  angular.module('app.util', [ 'util.router', 'util.data', 'util.captcha', 'util.camera', 'util.file']);
})();
