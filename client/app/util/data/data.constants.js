(function() {
  'use strict';

  angular
  .module('app.util')
  .constant("baseURL","http://localhost:3000/api")
  //.constant("baseURL", "http://capstone-juan-jordaan.eu-gb.mybluemix.net/api")
  .constant('httpVerbs', {'put': {method:'PUT'}, 'post': {method:'POST'}, 'list':  {method:'GET', isArray:true} } );
})();
