(function () {
  'use strict';

  angular
  .module('util.data')
  .factory('dataservice', dataservice);

  //, 'captchaKey'
  dataservice.$inject = ['$resource', 'baseURL', 'httpVerbs'];

  function dataservice($resource, baseURL, httpVerbs) {
    // var service = {
    //   countries: countries(),
    //   skills: skills(),
    //   register: register(),
    //   skills: projects()
    // };

    return {
      countries: countries,
      skills: skills,
      messages: messages,
      users: users,
      userSkills: userSkills,
      userRegister: userRegister,
      project: project,
      projectBid: projectBid,
      projectOwner: projectOwner,
      projectProvider: projectProvider
    };

    function countries( ){
      return $resource( baseURL+"/countries", null, httpVerbs );
    }

    function skills( ){
      return $resource( baseURL+"/skills", null, httpVerbs );
    }

    function messages( ){
      return $resource( baseURL+"/messages/user/:userId", null, httpVerbs );
    }

    function users( ){
      return $resource( baseURL+"/users/:userId", null, httpVerbs );
    }

    function userSkills( ){
      return $resource( baseURL+"/users/:userId/skills", null, httpVerbs );
    }

    function userRegister( ){
      return $resource( baseURL+"/users/register", null, httpVerbs );
    }

    function project( ){
      return $resource( baseURL+"/project/:id", null, httpVerbs );
    }

    function projectBid( ){
      return $resource( baseURL+"/project/:id/bid", null, httpVerbs );
    }

    function projectOwner(){
      return $resource( baseURL+"/owner/:companyName", null, httpVerbs );
    }

    function projectProvider(){
      return $resource( baseURL+"/provider/:userId", null, httpVerbs );
    }
  }
})();
