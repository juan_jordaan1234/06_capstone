(function() {
  'use strict';

  angular
  .module('app.dashboard.messages')
  .run(getRoute);

  getRoute.$inject = ['routeUtil'];

  function getRoute(routeUtil) {
    //console.log('sending route to util');
    routeUtil.addState(
      {
        name: 'dashboard.messages',
        config: {
          url: '/messages',
          // views: {
          //   'dashboard@': {
              templateUrl:'/app/dashboard/messages/messages.html',
              controller: 'MessagesController',
              controllerAs: 'vm',
              title:'Messages'
          //   }
          // }
        }
      }
    )
  }
})();
