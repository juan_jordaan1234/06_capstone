(function() {
  'use strict';

  angular
  .module('app.dashboard.provider')
  .run(getRoute);

  getRoute.$inject = ['routeUtil'];

  function getRoute(routeUtil) {
    //console.log('sending route to util');
    routeUtil.addState(
      {
        name: 'dashboard.provider',
        config: {
          url: '/provider',
          // views: {
          //   'dashboard@': {
              templateUrl:'/app/dashboard/provider/provider.html',
              controller: 'ProviderController',
              controllerAs: 'vm',
              title:'Service Provider'
          //   }
          // }
        }
      }
    )
  }
})();
