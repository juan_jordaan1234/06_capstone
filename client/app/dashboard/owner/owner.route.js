(function() {
  'use strict';

  angular
  .module('app.dashboard.owner')
  .run(getRoute);

  getRoute.$inject = ['routeUtil'];

  function getRoute(routeUtil) {
    //console.log('sending route to util');
    routeUtil.addState(
      {
        name: 'dashboard.owner',
        config: {
          url: '/owner',
          // views: {
          //   'dashboard@': {
              templateUrl:'/app/dashboard/owner/owner.html',
              controller: 'OwnerController',
              controllerAs: 'vm',
              title:'Project Owner'
          //   }
          // }
        }
      }
    )
  }
})();
