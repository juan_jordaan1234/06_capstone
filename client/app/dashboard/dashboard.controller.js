(function() {
  'use strict';

  angular
  .module('app.dashboard')
  .controller('DashboardController', DashboardController);

  DashboardController.$inject = ['$q', 'DashboardService'];

  function DashboardController($q, DashboardService) {
    var vm = this;
    vm.dashboardItems = DashboardService.getDashboardItems();

    console.log('dashboardItems = ' + angular.toJson(vm) );
  }
})();
