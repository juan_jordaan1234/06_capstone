(function() {
  'use strict';

  angular
  .module('app.dashboard')
  .factory('DashboardService', DashboardService);

  DashboardService.$inject = ['$q'];

  function DashboardService($q) {
    var fac = {};
    fac.getDashboardItems = getDashboardItems;
    return fac;

    function getDashboardItems(){
      var dashBoardItems = [
        {'id':'1', 'sref':"", 'badgeText' : "Item 1", 'img' : ""},
        {'id':'2', 'sref':"", 'badgeText' : "Item 2", 'img' : ""},
        {'id':'3', 'sref':"", 'badgeText' : "Item 3", 'img' : ""},
        {'id':'4', 'sref':"", 'badgeText' : "Item 4", 'img' : ""}
      ];

      console.log('dashBoardItems = ' + dashBoardItems );

      return dashBoardItems;
    }
  }
})();
