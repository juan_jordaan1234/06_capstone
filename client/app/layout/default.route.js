(function() {
  'use strict';

  angular
  .module('app.util')
  .run(getRoute);

  getRoute.$inject = ['routeUtil'];

  function getRoute(routeUtil) {
    //console.log('sending route to util');
    routeUtil.addState(
      {
        name: 'index',
        config: {
          url: '/',
          views: {
            // 'header': {
            //   templateUrl : '/app/layout/header2.html',
            // },
            'content': {
              templateUrl : '/app/home/home.html',
              controller: 'HomeController',
              controllerAs: 'vm',
              title: 'Home'
            },
            // 'footer': {
            //   templateUrl : '/app/layout/footer.html',
            // }
          }
        }
      }
    )
  }
})();
