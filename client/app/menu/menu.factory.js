(function() {
  'use strict';

  angular
  .module('app.menu')
  .service('menuFactory', menuFactory);

  menuFactory.$inject = ['$resource', 'baseURL', 'httpVerbs'];
  function menuFactory( $resource, baseURL, httpVerbs ) {
    this.getDishes = function() {
      return $resource( baseURL+"dishes/:id", null, httpVerbs );
    };

    this.getPromotions = function( ) {
      return $resource( baseURL+"promotions/:id", null, httpVerbs );
    };
  }
})();
