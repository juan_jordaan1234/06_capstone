(function() {
  'use strict';

  angular
  .module('app.menu')
  .run(getRoute);

  getRoute.$inject = ['routeUtil'];

  function getRoute(routeUtil) {
    //console.log('sending route to util');
    routeUtil.addState(
      {
        name: 'menu',
        config: {
          url: '/menu',
          views: {
            'content': {
              templateUrl: '/app/menu/menu.html',
              controller: 'MenuController',
              controllerAs: 'vm',
              title: 'Menu'
            }
          }
        }
      }
    )
  }
})();
