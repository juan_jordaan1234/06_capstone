(function() {
  'use strict';

  angular
  .module('app.menu')
  .controller('MenuController', MenuController);

  MenuController.$inject = ['$timeout'];

  function MenuController($timeout) {
    var vm = this;
    vm.tab = 1;
    vm.filtText = '';
    vm.showDetails = false;

    vm.showMenu = false;
    vm.dishes = [];
    vm.message="Loading ...";

    // vm.dishes = menuFactory.getDishes().query(
    //   function( response ){
    //     vm.dishes = response;
    //     vm.showMenu = true;
    //   },
    //   function( response ){
    //     vm.message = "Error: " + response.status + " " + response.statusText;
    //     console.error( vm.message );
    //   }
    // );

    // vm.select = function(setTab) {
    //   vm.tab = setTab;
    //
    //   if (setTab === 2) {
    //     vm.filtText = "appetizer";
    //   }
    //   else if (setTab === 3) {
    //     vm.filtText = "mains";
    //   }
    //   else if (setTab === 4) {
    //     vm.filtText = "dessert";
    //   }
    //   else {
    //     vm.filtText = "";
    //   }
    // };

    // vm.isSelected = function (checkTab) {
    //   return (vm.tab === checkTab);
    // };

    // vm.toggleDetails = function() {
    //   vm.showDetails = !vm.showDetails;
    // };
  }
})();
