(function() {
  'use strict';

  angular
  .module('app.members.message')
  .run(getRoute);

  getRoute.$inject = ['routeUtil'];

  function getRoute(routeUtil) {
    //console.log('sending route to util');
    routeUtil.addState(
      {
        name: 'app.members_message',
        config: {
          url: 'message',
          views: {
            'content@': {
              templateUrl:'/app/members/message/message.html',
              controller: 'LoginController',
              controllerAs: 'vm',
              title:'Login'
            }
          }
        }
      }
    )
  }
})();
