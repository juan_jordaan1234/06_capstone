// Thanks to Brian Ford
// https://github.com/btford/angular-socket-io-im

(function() {
  'use strict';

  angular
  .module('app.members.login')
  .factory('socket', socket);

  socket.$inject = ['$rootScope'];
  function socket($rootScope){
    var socket = io.connect();

    return {
      on: function (eventName, callback) {
        socket.on(eventName, function () {
          var args = arguments;
          $rootScope.$apply(function () {
            callback.apply(socket, args);
          });
        });
      },
      emit: function (eventName, data, callback) {
        socket.emit(eventName, data, function () {
          var args = arguments;
          $rootScope.$apply(function () {
            if (callback) {
              callback.apply(socket, args);
            }
          });
        })
      }
    }
  }
})();
