(function() {
  'use strict';

  angular
  .module('app.members.login')
  .run(getRoute);

  getRoute.$inject = ['routeUtil'];

  function getRoute(routeUtil) {
    //console.log('sending route to util');
    routeUtil.addState(
      {
        name: 'members_login',
        config: {
          url: '/login',
          views: {
            'content@': {
              templateUrl:'/app/members/login/login.html',
              controller: 'LoginController',
              controllerAs: 'vm',
              title:'Login'
            }
          }
        }
      }
    )
  }
})();
