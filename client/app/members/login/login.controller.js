(function() {
  'use strict';

  angular
  .module('app.members.login')
  .controller('LoginController', LoginController);

  //'AuthenticationService'
  LoginController.$inject = ['$location'];
  function LoginController($location) {
    var vm = this;

    vm.login = login;
    vm.username = '';
    vm.password = '';

    function login(){
      console.log ('credentials : username = ' + vm.username + " password = " + vm.password);

      // if(AuthenticationService.authenticate( vm.username, vm.password)) {
      //   console.log ('authenticate passed ');
      //   $location.path('dashboard');
      // }
      // else {
      //   console.log ('authenticate failed');
      // }

      $location.path('dashboard');
    }
  }
})();
