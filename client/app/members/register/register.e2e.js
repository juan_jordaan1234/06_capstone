'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('Register E2E Test', function() {
  // it('should automatically redirect to /view1 when location hash/fragment is empty', function() {
  //   browser.get('index.html');
  //   expect(browser.getLocationAbsUrl())
  //   .toMatch("/view1");
  // });

  it('should automatically redirect to / when location hash/fragment is empty', function() {
    browser.get('index.html');

    expect(browser.getLocationAbsUrl()).toMatch("/");
  });

  describe('register', function() {
    beforeEach(function() {
      browser.get('/#/register');
    });

    it('should redirect to /register', function() {
      browser.get('/#/register');
      expect(browser.getLocationAbsUrl()).toMatch("/register");
    });

    it('should register a user', function() {
      browser.get('/#/register');

      let country = element(by.model('vm.user.company'));
      element(by.model('vm.user.country')).sendKeys('Private');
      element(by.model('vm.user.password')).sendKeys('123asd');
      element(by.css('[id="passwordConfirm"]')).sendKeys('123asd');

      let countryList = element.all(by.repeater('country in vm.countries'));
      country = 'South Africa';

      for( let country in countryList){
        console.log('country = ' + country);
      }

      var submitButton = element(by.css('[type="submit"]'));

      submitButton.click();
    });
  });

  // describe('view2', function() {
  //   beforeEach(function() {
  //     browser.get('index.html#/view2');
  //   });
  //
  //   it('should render view2 when user navigates to /view2', function() {
  //     expect(element.all(by.css('[ng-view] p'))
  //     .first()
  //     .getText())
  //     .toMatch(/partial for view 2/);
  //   });
  // });
});
