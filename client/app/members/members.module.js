(function() {
  'use strict';

  angular.module('app.members', ['app.members.register', 'app.members.login', 'app.members.recover', 'app.members.profile']);
})();
