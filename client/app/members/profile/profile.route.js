(function() {
  'use strict';

  angular
  .module('app.members.profile')
  .run(getRoute);

  getRoute.$inject = ['routeUtil'];

  function getRoute(routeUtil) {
    //console.log('sending route to util');
    routeUtil.addState(
      {
        name: 'members_profile',
        config: {
          url: '/profile',
          views: {
            'content@': {
              templateUrl:'/app/members/profile/profile.html',
              controller: 'ProfileController',
              controllerAs: 'vm',
              title:'Profile'
            }
          }
        }
      }
    )
  }
})();
