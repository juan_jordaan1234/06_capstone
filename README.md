cd mongodb/
mongod -dbpath data

npm start

? slc arc ?

##Initial DB Setup
node server/config/db.setup.js

##Test
###Server - Rest Api Tests
npm run test
###Client - E2E Tests
npm run update-webdriver
npm run preprotractor
npm run protractor

##RAML
###raml2html raml/api.raml > capstone.html


## Host Application
npm start


##Deploy Bluemix
cd <directory>
###connect to bluemix
bluemix api https://api.eu-gb.bluemix.net

###Log in to Bluemix.
bluemix login -u juan.jordaan1234@gmail.com -o juan.jordaan1234@gmail.com -s dev

###Deploy your app
cf push capstone-juan-jordaan

###access app
http://capstone-juan-jordaan.mybluemix.net
